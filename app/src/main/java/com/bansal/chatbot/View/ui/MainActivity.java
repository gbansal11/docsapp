package com.bansal.chatbot.View.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bansal.chatbot.Model.DataModels.Message;
import com.bansal.chatbot.Model.SharedPrefrenceManager;
import com.bansal.chatbot.Presenter.HomePresenter;
import com.bansal.chatbot.R;
import com.bansal.chatbot.SyncMessageIntentService;
import com.bansal.chatbot.View.ViewPresenterContract.HomePresenterContract;
import com.bansal.chatbot.adapters.MyAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements HomePresenterContract.View {


    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    public RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Message> messageArrayList = new ArrayList<Message>();
    private Button send;
    private EditText messageText;
    public BroadcastReceiver receiver;


    private HomePresenterContract.Presenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            send = (Button) findViewById(R.id.button_send);
            messageText = (EditText) findViewById(R.id.chat_text);
            send.setOnClickListener(this);

            mRecyclerView = (RecyclerView) findViewById(R.id.message_list); // Assigning the RecyclerView Object to the xml View
            mRecyclerView.setHasFixedSize(true);


            messageArrayList=SharedPrefrenceManager.getMessages().getMessages();
            mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter =new MyAdapter(messageArrayList,this);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    // Call smooth scroll
                    mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
                }
            });

            presenter =new HomePresenter(this);

            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d("TEST","localbroadcast");

                    refreshList();

                }
            };


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View view) {

        try {
            switch (view.getId())
            {
                case R.id.button_send:
                    if(!messageText.getText().toString().equals("")){
                    presenter.sendMessage(messageText.getText().toString());}

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TEST","onresume");

        refreshList();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(messageText, InputMethodManager.SHOW_IMPLICIT);



    }

    @Override
    public void updateChat(Message message) {

        messageArrayList.add(message);
        Log.d("TEST","update");
        mAdapter.notifyDataSetChanged();
        messageText.setText("");

        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                // Call smooth scroll
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(SyncMessageIntentService.DATA_CHANGED)
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
    }


    public void refreshList ()
    {
        Log.d("TEST","refreshlist");

        messageArrayList.clear();
        messageArrayList.addAll(SharedPrefrenceManager.getMessages().getMessages());
        mAdapter.notifyDataSetChanged();
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                // Call smooth scroll
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
            }
        });
    }
}
