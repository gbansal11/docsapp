package com.bansal.chatbot.View.ViewPresenterContract;

import com.bansal.chatbot.Model.DataModels.Message;


public interface HomePresenterContract {

    interface View extends android.view.View.OnClickListener {

      void updateChat(Message message);
        void showError(String error);


    }

    interface Presenter  {

        void sendMessage(String messageText);


    }
}
