package com.bansal.chatbot;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bansal.chatbot.Model.DataManager;
import com.bansal.chatbot.Model.DataModels.Message;
import com.bansal.chatbot.Model.DataModels.ResponseDataModels;
import com.bansal.chatbot.Model.SharedPrefrenceManager;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SyncMessageIntentService extends IntentService implements Response.Listener<ResponseDataModels>,Response.ErrorListener {

   public static final  String DATA_CHANGED = "data_change";
  public LocalBroadcastManager broadcaster;
    public SyncMessageIntentService() {
        super("SyncMessageIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        broadcaster = LocalBroadcastManager.getInstance(this);

        try {
            Log.d("TEST","insideservice called");

                DataManager.getResponse(this, SharedPrefrenceManager.getLeftMessage(), this, this);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(ResponseDataModels response) {

        if(response.getSuccess()==1)
        {
            Message message = response.getMessage();
            message.setType(2);
            SharedPrefrenceManager.updateMesages(message);

            Log.d("TEST","message added");

            Intent intent = new Intent(DATA_CHANGED);
            broadcaster.sendBroadcast(intent);


        }
    }


}
