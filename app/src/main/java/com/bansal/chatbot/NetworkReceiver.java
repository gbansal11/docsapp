package com.bansal.chatbot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.bansal.chatbot.Model.SharedPrefrenceManager;

/**
 * Created by BANSAL on 25/02/17.
 */

public class NetworkReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("TEST","broadcastreceiver invoked");


        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            final ConnectivityManager conMgr =  (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
            boolean status= activeNetwork != null && activeNetwork.getState() == NetworkInfo.State.CONNECTED;

            if(status)
            {
                Log.d("TEST","broadcastreceiver");
                if(!SharedPrefrenceManager.getLeftMessage().equals("")) {
                    Log.d("TEST","service called");

                    Intent intent1 = new Intent(context, SyncMessageIntentService.class);
                    context.startService(intent1);
                }
            }
        }
    }
}
