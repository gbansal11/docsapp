package com.bansal.chatbot.adapters;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bansal.chatbot.Model.DataModels.Message;
import com.bansal.chatbot.R;

import java.util.ArrayList;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private  Activity mActivity;
    private ArrayList<Message> messages;

    public MyAdapter(ArrayList<Message> messages, Activity mActivity) {
        this.messages = messages;
        this.mActivity=mActivity;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v;
        if(viewType== Message.SEND_TYPE) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_view_right, parent, false);
        }
        else
        {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_view_left, parent, false);
        }
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.messgae.setText(messages.get(position).getMessage());



    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView messgae;
        public ViewHolder(View v) {
            super(v);
            messgae= (TextView) v.findViewById(R.id.message_text);


        }
    }

    @Override
    public int getItemViewType(int position) {

        if(messages.get(position).getType()==1)
        {return Message.SEND_TYPE;}
        else
            return Message.RECEIVE_TYPE;

    }
}



