package com.bansal.chatbot.Model.DataModels;

import java.util.ArrayList;

/**
 * Created by BANSAL on 25/02/17.
 */

public class MessageList {

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    private ArrayList<Message> messages = new ArrayList<Message>();
}
