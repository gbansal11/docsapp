package com.bansal.chatbot.Model.DataModels;

/**
 * Created by BANSAL on 25/02/17.
 */

public class ResponseDataModels {

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    private int success;
    private String errorMessage;
    private Message message;

}
