package com.bansal.chatbot.Model;

import android.content.Context;
import android.content.SharedPreferences;

import com.bansal.chatbot.Model.DataModels.Message;
import com.bansal.chatbot.Model.DataModels.MessageList;
import com.bansal.chatbot.MyApplication;
import com.google.gson.Gson;

/**
 * Created by BANSAL on 25/02/17.
 */

public class SharedPrefrenceManager {

    public static final String USER_PREFRENCES= "user_pref";
    public static final String MESSAGE_LIST= "message_list";
    public static final String MESSAGE_LEFT= "message_left";



    public static MessageList getMessages( )

    {
        try {
            SharedPreferences prefs= MyApplication.getAppContext().getSharedPreferences(USER_PREFRENCES,Context.MODE_PRIVATE);
            //  SharedPreferences.Editor editor= prefs.edit();
            String messageList =prefs.getString(MESSAGE_LIST, "");
            if(messageList!=null && !messageList.equals("")) {
                Gson gson = new Gson();
                MessageList messageList1 = gson.fromJson(messageList, MessageList.class);

                return messageList1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new MessageList();
    }
    public static  void updateMesages(Message message )

    {
        try {
            SharedPreferences prefs= MyApplication.getAppContext().getSharedPreferences(USER_PREFRENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor= prefs.edit();
            String messageListJson =prefs.getString(MESSAGE_LIST, "");
            Gson gson = new Gson();
            MessageList messageList=new MessageList();
            if(!messageListJson
                    .equals(""))
            {
                messageList=gson.fromJson(messageListJson, MessageList.class);}


            messageList.getMessages().add(message);

            messageListJson = gson.toJson(messageList);
            editor.putString(MESSAGE_LIST,messageListJson);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String getLeftMessage( )

    {
        try {
            SharedPreferences prefs= MyApplication.getAppContext().getSharedPreferences(USER_PREFRENCES,Context.MODE_PRIVATE);
            //  SharedPreferences.Editor editor= prefs.edit();
            String messageList =prefs.getString(MESSAGE_LEFT, "");

            return messageList;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static  void saveMessageLeft(String message )

    {
        try {
            SharedPreferences prefs= MyApplication.getAppContext().getSharedPreferences(USER_PREFRENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor= prefs.edit();
            String messageListJson =prefs.getString(MESSAGE_LEFT, "");

            editor.putString(MESSAGE_LEFT,messageListJson+message);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
