package com.bansal.chatbot.Model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bansal.chatbot.Model.DataModels.ResponseDataModels;
import com.bansal.chatbot.MyApplication;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by BANSAL on 25/02/17.
 */

public class DataManager {
    private static final String BASE_URL="http://www.personalityforge.com/api/chat/";
    private static final String API_KEY="6nt5d1nJHkqbkphe";
    private static final String CHAT_BOT_ID="63906";
    private static final String EXTERNAL_ID="chirag1";



    public static void getResponse(Context context, final String message, final Response.Listener <ResponseDataModels> listener, final Response.ErrorListener errorListener)

    {
        Log.d("TEST","" +"json request" +
                "");

        RequestQueue queue = MyApplication.getInstance().getRequestQueue();

        Log.d("URL",getUrl(message));

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, getUrl(message), null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.d("Response", response.toString());
                        Gson gson= new Gson();

                        ResponseDataModels responseDataModels = gson.fromJson(response.toString(),ResponseDataModels.class);

                        listener.onResponse(responseDataModels);

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                        errorListener.onErrorResponse(error);
                        SharedPrefrenceManager.saveMessageLeft(message);
                    }
                }
        );

// add it to the RequestQueue
        queue.add(getRequest);

    }

    //http://www.personalityforge.com/api/chat/?apiKey=6nt5d1nJHkqbkphe&message=Hi&chatBotID=63906&externalID=chirag1
    private static String getUrl(String message) {
        try {
            String m=URLEncoder.encode(message, "utf-8");
            return BASE_URL
                    + "?apiKey=" + API_KEY
                    + "&message=" + m
                    + "&chatBotID=" + CHAT_BOT_ID
                    + "&externalID=" + EXTERNAL_ID;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

}
