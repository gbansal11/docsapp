package com.bansal.chatbot.Model.DataModels;

import com.android.volley.VolleyError;

/**
 * Created by BANSAL on 25/02/17.
 */

public class VolleyErrorCustom {

    public VolleyError getVolleyError() {
        return volleyError;
    }

    public void setVolleyError(VolleyError volleyError) {
        this.volleyError = volleyError;
    }

    private VolleyError volleyError;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
}
