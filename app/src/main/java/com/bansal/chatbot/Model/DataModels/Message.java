package com.bansal.chatbot.Model.DataModels;

/**
 * Created by BANSAL on 25/02/17.
 */

public class Message {
    public static final int SEND_TYPE=1;
    public static final int RECEIVE_TYPE=2;
    public String getChatBotName() {
        return chatBotName;
    }

    public void setChatBotName(String chatBotName) {
        this.chatBotName = chatBotName;
    }

    public String getChatBotId() {
        return chatBotId;
    }

    public void setChatBotId(String chatBotId) {
        this.chatBotId = chatBotId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    private String chatBotName;
    private String chatBotId;
    private String message;
    private String emotion;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    private int type;
}
