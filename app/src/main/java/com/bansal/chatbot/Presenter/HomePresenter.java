package com.bansal.chatbot.Presenter;

import android.content.Context;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bansal.chatbot.Model.DataManager;
import com.bansal.chatbot.Model.DataModels.Message;
import com.bansal.chatbot.Model.DataModels.ResponseDataModels;
import com.bansal.chatbot.Model.SharedPrefrenceManager;
import com.bansal.chatbot.View.ViewPresenterContract.HomePresenterContract;

public class HomePresenter implements HomePresenterContract.Presenter,Response.Listener<ResponseDataModels>,Response.ErrorListener{

    public HomePresenterContract.View view;



    public HomePresenter(HomePresenterContract.View view)
    {
        this.view=view;
    }


    @Override
    public void sendMessage(String messageText) {

        try {
            Message message = new Message();
            message.setType(1);
            message.setMessage(messageText);
            SharedPrefrenceManager.updateMesages(message);
            view.updateChat(message);

            DataManager.getResponse((Context)view,messageText,this,this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onErrorResponse(VolleyError error) {
        if (error instanceof NetworkError || error instanceof NoConnectionError)
        {
           // SharedPrefrenceManager.saveMessageLeft();
            view.showError("No Network Connection");
            return;

        }
        view.showError(error.toString());

    }

    @Override
    public void onResponse(ResponseDataModels response) {

        if(response.getSuccess()==1)
        {
            Message message = response.getMessage();
            message.setType(2);
            SharedPrefrenceManager.updateMesages(message);
            view.updateChat(message);

        }

    }
}
